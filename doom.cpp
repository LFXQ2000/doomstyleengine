#include <SDL2/SDL.h>
#include <iostream>
#include <vector>
#include <math.h>
#include <chrono>
#include <algorithm>
#include <fstream>
#include "maths.hpp"

constexpr int sWidth = 640;
constexpr int sHeight = 480;
constexpr int pixSize = 2;

float fov = 1.7f;
float nearplane = 4.0f;

constexpr float turnspeed = 3.0f;
constexpr float movespeed = 128.0f;

constexpr float pi = 3.14159265359f;

constexpr float camHeight = 48.0f;

float wrapAngle(float angle)
{
    if(angle < 0)
    {
        angle += pi*2;
    }
    else if(angle > pi*2)
    {
        angle -= pi*2;
    }
    return angle;
}

template<typename T>
struct Vec2
{
    T x,y;
};

typedef Vec2<float> Vec2f;
typedef Vec2<int> Vec2i;

constexpr Vec2i pOrigin = {int(sWidth*0.5f),int(sHeight*0.55f)};

struct color
{
    uint8_t r,g,b;
};

color fBuf[sWidth*sHeight];
float zBuf[sWidth*sHeight];

constexpr int getPos(int x, int y)
{
    return (y*sWidth) + x;
}

struct player
{
    Vec2f pos = {0.0f,0.0f};
    float height = camHeight;
    float angle = 0.0f;
    int cursec = 0;
} pl;

void PutPixel(int x, int y, color c)
{
    if(x >= 0 && x < sWidth && y >= 0 && y < sHeight)
    {
        fBuf[getPos(x,y)] = c;
    }
}

void PutPixelZ(int x, int y, color c, float z)
{
    if(x >= 0 && x < sWidth && y >= 0 && y < sHeight && z < zBuf[getPos(x,y)])
    {
        fBuf[getPos(x,y)] = c;
        zBuf[getPos(x,y)] = z;
    }
}

void PutZ(int x, int y, float z)
{
    if(x >= 0 && x < sWidth && y >= 0 && y < sHeight)
    {
        zBuf[getPos(x,y)] = z;
    }
}

struct line
{
    Vec2f p1;
    Vec2f p2;
    color c;
};

struct sector
{
    float floor;
    float ceiling;
    color floorColor;
    color ceilingColor;
};

struct linedef
{
    int p1,p2;
    int frontsec,backsec;
    bool fb,fm,ft;
    bool bb,bm,bt;
    color cfb,cfm,cft;
    color cbb,cbm,cbt;
};

std::vector<Vec2f> Vertices
{
    {-256.0f,-256.0f}, //0
    {256.0f,-256.0f}, //1
    {300.0f,200.0f}, //2
    {-100.0f,200.0f}, //3
    {-500.0f,50.0f}, //4
    {-600.0f,-100.0f}, //5
    {-64.0f,32.0f}, //6
    {64.0f,32.0f}, //7
    {64.0f,-96.0f}, //8
    {-64.0f,-96.0f} //9
};

std::vector<linedef> Linedefs
{
    {
        0,1,      //points
        2,-1,     //front sector, back sector
        0,1,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {255,0,0},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        1,2,      //points
        1,-1,     //front sector, back sector
        0,1,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {255,255,0},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        2,3,      //points
        0,-1,     //front sector, back sector
        0,1,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {0,255,0},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        3,4,      //points
        3,-1,     //front sector, back sector
        0,1,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {0,255,255},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        4,5,      //points
        3,-1,     //front sector, back sector
        0,1,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {0,0,255},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        5,0,      //points
        3,-1,     //front sector, back sector
        0,1,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {255,0,255},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },

    {
        3,6,      //points
        0,3,     //front sector, back sector
        1,0,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {128,255,255},    //front bottom color
        {0,0,0},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        2,7,      //points
        1,0,     //front sector, back sector
        0,0,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {0,0,0},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        1,8,      //points
        2,1,     //front sector, back sector
        0,0,0,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {0,0,0},  //front middle color
        {0,0,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        0,9,      //points
        3,2,     //front sector, back sector
        0,0,0,    //enable front bottom, middle, top
        1,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {0,0,0},  //front middle color
        {0,0,0},    //front top color
        {128,128,255},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },

    {
        6,7,      //points
        0,4,     //front sector, back sector
        1,0,1,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {128,128,255},    //front bottom color
        {0,0,0},  //front middle color
        {128,128,255},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        7,8,      //points
        1,4,     //front sector, back sector
        1,0,1,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {255,128,128},    //front bottom color
        {0,0,0},  //front middle color
        {255,128,128},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        8,9,      //points
        2,4,     //front sector, back sector
        1,0,1,    //enable front bottom, middle, top
        0,0,0,    //enable back bottom, middle, top
        {128,255,0},    //front bottom color
        {0,0,0},  //front middle color
        {128,255,0},    //front top color
        {0,0,0},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
    {
        9,6,      //points
        3,4,     //front sector, back sector
        0,0,1,    //enable front bottom, middle, top
        1,0,0,    //enable back bottom, middle, top
        {0,0,0},    //front bottom color
        {0,0,0},  //front middle color
        {255,128,255},    //front top color
        {255,128,255},    //back bottom color
        {0,0,0},  //back middle color
        {0,0,0},    //back top color
    },
};

std::vector<sector> Sectors
{
    {0.0f,128.0f,{0,0,128},{128,128,128}},
    {0.0f,128.0f,{0,32,128},{128,128,96}},
    {0.0f,128.0f,{0,64,128},{128,128,64}},
    {32.0f,128.0f,{0,96,128},{128,128,32}},
    {16.0f,112.0f,{128,0,128},{64,64,64}}
};

Vec2f vecPlayerTransform(const Vec2f& vec)
{
    Vec2f tvec = {   vec.x - pl.pos.x,
                    vec.y - pl.pos.y};
    tvec = { tvec.x * cosf(-pl.angle) - tvec.y * sinf(-pl.angle),
            tvec.x * sinf(-pl.angle) + tvec.y * cosf(-pl.angle)};
    return tvec;
}

void drawLine(const line& l)
{
    float x1 = l.p1.x;
    float x2 = l.p2.x;
    float y1 = l.p1.y;
    float y2 = l.p2.y;
    if(x1 > x2)
    {
        std::swap(x1,x2);
        std::swap(y1,y2);
    }
    float dy = (y2-y1)/(x2-x1);
    if(abs(dy)>1.0f)
    {
        if(y1 > y2)
        {
            std::swap(x1,x2);
            std::swap(y1,y2);
        }
        float dx = (x2-x1)/(y2-y1);
        float x = x1;
        for(float y = y1;y<y2;y++)
        {
            PutPixel(floor(x),floor(y),l.c);
            x+=dx;
        }
    }
    else
    {
        float y = y1;
        for(float x = x1;x<x2;x++)
        {
            PutPixel(floor(x),floor(y),l.c);
            y+=dy;
        }
    }
}

void drawLineTop(const line& l)
{
    float x1 = l.p1.x;
    float x2 = l.p2.x;
    float y1 = l.p1.y;
    float y2 = l.p2.y;
    if(x1 > x2)
    {
        std::swap(x1,x2);
        std::swap(y1,y2);
    }
    float dy = (y2-y1)/(x2-x1);
    if(abs(dy)>1.0f)
    {
        if(y1 > y2)
        {
            std::swap(x1,x2);
            std::swap(y1,y2);
        }
        float dx = (x2-x1)/(y2-y1);
        float x = x1;
        for(float y = y1;y<y2;y++)
        {
            PutPixel(floor(x),floor(y),l.c);
            PutZ(floor(x),floor(y),0);
            x+=dx;
        }
    }
    else
    {
        float y = y1;
        for(float x = x1;x<x2;x++)
        {
            PutPixel(floor(x),floor(y),l.c);
            PutZ(floor(x),floor(y),0);
            y+=dy;
        }
    }
}

struct
{
    bool left = false;
    bool right = false;
    bool up = false;
    bool down = false;
    bool plus = false;
    bool minus = false;
    bool f12 = false;
}keys;

void drawWall2D(const line& l, float f, float c)
{
    line tl = {vecPlayerTransform(l.p1),vecPlayerTransform(l.p2),l.c};


    
    tl.p1.y *= -1;
    tl.p2.y *= -1;

    if(tl.p1.y < nearplane && tl.p2.y < nearplane)
    {
        return;
    }

    if(tl.p1.y < nearplane)
    {
        const float ratio = (tl.p2.y-nearplane)/(tl.p2.y-tl.p1.y);
        tl.p1.x = tl.p2.x - ((tl.p2.x-tl.p1.x)*ratio);
        tl.p1.y = nearplane;
    }
    else if(tl.p2.y < nearplane)
    {
        const float ratio = (tl.p1.y-nearplane)/(tl.p1.y-tl.p2.y);
        tl.p2.x = tl.p1.x - ((tl.p1.x-tl.p2.x)*ratio);
        tl.p2.y = nearplane;
    }

    tl.p1.y *= -1;
    tl.p2.y *= -1;

    tl.p1.x += pOrigin.x;
    tl.p1.y += pOrigin.y;
    tl.p2.x += pOrigin.x;
    tl.p2.y += pOrigin.y;
    drawLine(tl);
}

void drawWall3D(const line& l, float f, float c)
{
    line tl = {vecPlayerTransform(l.p1),vecPlayerTransform(l.p2),l.c};

    Vec2f normal = {(tl.p2.y-tl.p1.y)*-1,(tl.p2.x-tl.p1.x)};

    float len = sqrtf(normal.x*normal.x+normal.y*normal.y);

    normal.x /= len;
    normal.y /= len;

    if((normal.x*tl.p1.x)+(normal.y*tl.p1.y)>0)
    {
        return;
    }

    tl.p1.y *= -1;
    tl.p2.y *= -1;

    if(tl.p1.y < nearplane && tl.p2.y < nearplane)
    {
        return;
    }

    if(tl.p1.y < nearplane)
    {
        const float ratio = (tl.p2.y-nearplane)/(tl.p2.y-tl.p1.y);
        tl.p1.x = tl.p2.x - ((tl.p2.x-tl.p1.x)*ratio);
        tl.p1.y = nearplane;
    }
    else if(tl.p2.y < nearplane)
    {
        const float ratio = (tl.p1.y-nearplane)/(tl.p1.y-tl.p2.y);
        tl.p2.x = tl.p1.x - ((tl.p1.x-tl.p2.x)*ratio);
        tl.p2.y = nearplane;
    }

    const float atf = atan(fov/700.0f);

    float zd1 = 1.0f/(tl.p1.y*atf);
    float zd2 = 1.0f/(tl.p2.y*atf);
    float x1 = (tl.p1.x * zd1)+sWidth/2.0f;
    float x2 = (tl.p2.x * zd2)+sWidth/2.0f;

    float yt1 = ((c-pl.height) * zd1)+sHeight/2.0f;
    float yb1 = ((f-pl.height) * zd1)+sHeight/2.0f;

    float yt2 = ((c-pl.height) * zd2)+sHeight/2.0f;
    float yb2 = ((f-pl.height) * zd2)+sHeight/2.0f;

    float z1 = tl.p1.y;
    float z2 = tl.p2.y;

    if((x1 < 0 && x2 < 0)||(x1 >= sWidth && x2 >= sWidth))
    {
        return;
    }
    if(x1 < 0)
    {
        const float ratio = x2/(x2-x1);
        yt1 = yt2 - ((yt2-yt1)*ratio);
        yb1 = yb2 - ((yb2-yb1)*ratio);
        z1 = z2 - ((z2-z1)*ratio);
        x1 = 0;
    }
    if(x2 > sWidth)
    {
        const float ratio = (x1-sWidth)/(x1-x2);
        yt2 = yt1 - ((yt1-yt2)*ratio);
        yb2 = yb1 - ((yb1-yb2)*ratio);
        z2 = z1 - ((z1-z2)*ratio);
        x2 = sWidth;
    }
    /*
    drawLineTop(
        {Vec2f{x1,(sHeight-1)-yt1},Vec2f{x2,(sHeight-1)-yt2},{255,255,255}}
    );
    drawLineTop(
        {Vec2f{x1,(sHeight-1)-yb1},Vec2f{x2,(sHeight-1)-yb2},{255,255,255}}
    );
    drawLineTop(
        {Vec2f{x1,(sHeight-1)-yt1},Vec2f{x1,(sHeight-1)-yb1},{255,255,255}}
    );
    drawLineTop(
        {Vec2f{x2,(sHeight-1)-yt2},Vec2f{x2,(sHeight-1)-yb2},{255,255,255}}
    );
    */


    if(int(x1)==int(x2))
    {
        if(yt1-yb1 < yt2-yb2)
        {
            std::swap(yt1,yt2);
            std::swap(yb1,yb2);
            std::swap(z1,z2);
        }
        for(float y = yt1;y<yb1;y++)
        {
            PutPixelZ(int(x1),int(y),l.c,z1);
        }
    }
    else
    {
        float dty = (yt2-yt1)/(x2-x1);
        float dby = (yb2-yb1)/(x2-x1);
        float curyt = yt1;
        float curyb = yb1;
        float x = x1;
        float z = z1;
        float dz = (z2-z1)/(x2-x1);
        for(x = x1;x<x2;x++)
        {
            float zf = std::clamp(1.0f-(z/1024.0f),0.01f,1.0f);
            color fogc = {uint8_t((float)l.c.r*zf),
                        uint8_t((float)l.c.g*zf),
                        uint8_t((float)l.c.b*zf)};
            for(int y=int(curyb);y<int(curyt);y++)
            {
                PutPixelZ(x,(sHeight-1)-y,fogc,z);
            }
            curyt = curyt+dty;
            curyb = curyb+dby;
            z += dz;
        }
    }
}

void drawFlats(const line& l, int sec)
{
    line tl = {vecPlayerTransform(l.p1),vecPlayerTransform(l.p2),l.c};

    Vec2f normal = {(tl.p2.y-tl.p1.y)*-1,(tl.p2.x-tl.p1.x)};

    float len = sqrtf(normal.x*normal.x+normal.y*normal.y);

    normal.x /= len;
    normal.y /= len;

    if((normal.x*tl.p1.x)+(normal.y*tl.p1.y)>0)
    {
        return;
    }

    tl.p1.y *= -1;
    tl.p2.y *= -1;

    if(tl.p1.y < nearplane && tl.p2.y < nearplane)
    {
        return;
    }

    if(tl.p1.y < nearplane)
    {
        const float ratio = (tl.p2.y-nearplane)/(tl.p2.y-tl.p1.y);
        tl.p1.x = tl.p2.x - ((tl.p2.x-tl.p1.x)*ratio);
        tl.p1.y = nearplane;
    }
    else if(tl.p2.y < nearplane)
    {
        const float ratio = (tl.p1.y-nearplane)/(tl.p1.y-tl.p2.y);
        tl.p2.x = tl.p1.x - ((tl.p1.x-tl.p2.x)*ratio);
        tl.p2.y = nearplane;
    }

    const float atf = atan(fov/700.0f);

    float zd1 = 1.0f/(tl.p1.y*atf);
    float zd2 = 1.0f/(tl.p2.y*atf);
    float x1 = (tl.p1.x * zd1)+sWidth/2.0f;
    float x2 = (tl.p2.x * zd2)+sWidth/2.0f;

    if(int(x1)!=int(x2))
    {

        float c = Sectors[sec].ceiling;
        float f = Sectors[sec].floor;

        float yt1 = ((c-pl.height) * zd1)+sHeight/2.0f;
        float yb1 = ((f-pl.height) * zd1)+sHeight/2.0f;

        float yt2 = ((c-pl.height) * zd2)+sHeight/2.0f;
        float yb2 = ((f-pl.height) * zd2)+sHeight/2.0f;

        float z1 = tl.p1.y;
        float z2 = tl.p2.y;

        if((x1 < 0 && x2 < 0)||(x1 >= sWidth && x2 >= sWidth))
        {
            return;
        }
        if(x1 < 0)
        {
            const float ratio = x2/(x2-x1);
            yt1 = yt2 - ((yt2-yt1)*ratio);
            yb1 = yb2 - ((yb2-yb1)*ratio);
            z1 = z2 - ((z2-z1)*ratio);
            x1 = 0;
        }
        if(x2 > sWidth)
        {
            const float ratio = (x1-sWidth)/(x1-x2);
            yt2 = yt1 - ((yt1-yt2)*ratio);
            yb2 = yb1 - ((yb1-yb2)*ratio);
            z2 = z1 - ((z1-z2)*ratio);
            x2 = sWidth;
        }
        float dty = (yt2-yt1)/(x2-x1);
        float dby = (yb2-yb1)/(x2-x1);
        float curyt = yt1;
        float curyb = yb1;
        float x = x1;
        float z = z1;
        float dz = (z2-z1)/(x2-x1);
        
        for(x = x1;x<x2;x++)
        {
            uint8_t cz = (uint8_t)std::clamp(z/4.0f,0.0f,255.0f);
            for(int y=int(curyb);y>=0;y--)
            {
                PutPixelZ(x,(sHeight-1)-y,Sectors[sec].floorColor,z);
            }
            for(int y=int(curyt);y<sHeight;y++)
            {
                PutPixelZ(x,(sHeight-1)-y,Sectors[sec].ceilingColor,z);
            }
            curyt = curyt+dty;
            curyb = curyb+dby;
            z += dz;
        }
    }
}

void drawLinedefs()
{
    for(auto l : Linedefs)
    {
        line lin = {Vertices[l.p1],Vertices[l.p2],{0,0,0}};
        if(l.frontsec>=0)
        {
            if(l.fb||l.fm||l.ft)
            {
                if(l.backsec>=0)
                {
                    if(l.fb)
                    {
                        lin.c = l.cfb;
                        drawWall3D(lin,Sectors[l.frontsec].floor,Sectors[l.backsec].floor);
                    }
                    if(l.ft)
                    {
                        lin.c = l.cft;
                        drawWall3D(lin,Sectors[l.backsec].ceiling,Sectors[l.frontsec].ceiling);
                    }
                }
                if(l.fm)
                {
                    lin.c = l.cfm;
                    drawWall3D(lin,Sectors[l.frontsec].floor,Sectors[l.frontsec].ceiling);
                }
            }
            drawFlats(lin,l.frontsec);
        }
        lin.p1 = Vertices[l.p2];
        lin.p2 = Vertices[l.p1];
        if(l.backsec>=0)
        {
            if(l.bb||l.bm||l.bt)
            {
                if(l.frontsec>=0)
                {
                    if(l.bb)
                    {
                        lin.c = l.cbb;
                        drawWall3D(lin,Sectors[l.backsec].floor,Sectors[l.frontsec].floor);
                    }
                    if(l.bt)
                    {
                        lin.c = l.cbt;
                        drawWall3D(lin,Sectors[l.frontsec].ceiling,Sectors[l.backsec].ceiling);
                    }
                }
                if(l.bm)
                {
                    lin.c = l.cbm;
                    drawWall3D(lin,Sectors[l.backsec].floor,Sectors[l.backsec].ceiling);
                }
            }
            drawFlats(lin,l.backsec);
        }
    }
}

int CastRay(const std::vector<line>& lines,const Vec2f pos)
{
    int intersections = 0;
    for(auto& l : lines)
    {
        int maxX = l.p1.x > l.p2.x ? int(l.p1.x+1) : int(l.p2.x+1);
        if (maths::doIntersect({(int)l.p1.x,(int)l.p1.y},{(int)l.p2.x,(int)l.p2.y},{(int)pos.x,(int)pos.y},{maxX,(int)pos.y}))
        {
            intersections++;
        }
    }
    return intersections;
}

int totalI = 0;

void DetectSector()
{
    for(int s=0;s<Sectors.size();s++)
    {
        std::vector<line> lines;
        for(auto& l : Linedefs)
        {
            if(l.backsec == s || l.frontsec == s)
            {
                lines.push_back({Vertices[l.p1],Vertices[l.p2],{0,0,0}});
            }
        }
        const int playerInt = CastRay(lines,pl.pos);
        //printf("Sec: %u - Intersections: %u\n",s,playerInt);
        if(playerInt % 2 == 1 && playerInt > 0)
        {
            pl.cursec = s;
            pl.height = Sectors[s].floor+camHeight;
            totalI = playerInt;
            break;
        }
    }

}

uint8_t zBufOut[sWidth*sHeight];

int main(int argc,char** argv)
{
    SDL_Window* window = NULL;
    SDL_Surface* screenSurface = NULL;
    if(SDL_Init(SDL_INIT_VIDEO)<0)
    {
        std::cout << "Couldn't initialize SDL: " << SDL_GetError() << std::endl;
    }
    else
    {
        window = SDL_CreateWindow("I have a penis", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, sWidth*pixSize, sHeight*pixSize, SDL_WINDOW_SHOWN);
        if(window == NULL)
        {
            std::cout << "Couldn't create window: " << SDL_GetError() << std::endl;
        }
        else
        {
            screenSurface = SDL_GetWindowSurface(window);
            bool quit = false;
            SDL_Event e;
            float dtime = 0.02f;
            std::chrono::high_resolution_clock clock;
            while(!quit)
            {
                const auto starttime = clock.now();
                while(SDL_PollEvent(&e) != 0)
                {
                    switch(e.type)
                    {
                        case SDL_QUIT:
                            quit = true;
                            break;
                        case SDL_KEYDOWN:
                            switch(e.key.keysym.sym)
                            {
                                case SDLK_LEFT:
                                    keys.left = true;
                                    break;
                                case SDLK_RIGHT:
                                    keys.right = true;
                                    break;
                                case SDLK_UP:
                                    keys.up = true;
                                    break;
                                case SDLK_DOWN:
                                    keys.down = true;
                                    break;
                                case SDLK_EQUALS:
                                    keys.plus = true;
                                    break;
                                case SDLK_MINUS:
                                    keys.minus = true;
                                    break;
                                case SDLK_F12:
                                    keys.f12 = true;
                                    break;
                            }
                            break;
                        case SDL_KEYUP:
                            switch(e.key.keysym.sym)
                            {
                                case SDLK_LEFT:
                                    keys.left = false;
                                    break;
                                case SDLK_RIGHT:
                                    keys.right = false;
                                    break;
                                case SDLK_UP:
                                    keys.up = false;
                                    break;
                                case SDLK_DOWN:
                                    keys.down = false;
                                    break;
                                case SDLK_EQUALS:
                                    keys.plus = false;
                                    break;
                                case SDLK_MINUS:
                                    keys.minus = false;
                                    break;
                            }
                            break;
                    }

                }

                if(keys.left)
                {
                    pl.angle = wrapAngle(pl.angle-(turnspeed*dtime));
                }
                if(keys.right)
                {
                    pl.angle = wrapAngle(pl.angle+(turnspeed*dtime));
                }
                if(keys.up)
                {
                    pl.pos.x += movespeed * sinf(pl.angle) * dtime;
                    pl.pos.y -= movespeed * cosf(pl.angle) * dtime;
                }
                if(keys.down)
                {
                    pl.pos.x -= movespeed * sinf(pl.angle) * dtime;
                    pl.pos.y += movespeed * cosf(pl.angle) * dtime;
                }

                if (keys.plus)
                {
                    fov += 1.0 * dtime;
                }

                if (keys.minus)
                {
                    fov -= 1.0 * dtime;
                }

                for(int i=0;i<sWidth*sHeight;i++)
                {
                    fBuf[i] = {0,0,0};
                    zBuf[i] = 1e38;
                }
                SDL_FillRect(screenSurface,NULL,0);

                DetectSector();

                drawLinedefs();

                //fBuf[getPos(pOrigin.x,pOrigin.y)] = {255,255,255};

                for(int y=0;y<sHeight;y++)
                {
                    for(int x=0;x<sWidth;x++)
                    {
                        SDL_Rect r =
                        {
                            x*pixSize,
                            y*pixSize,
                            pixSize,
                            pixSize
                        };
                        color c = fBuf[getPos(x,y)];
                        SDL_FillRect(screenSurface,&r,SDL_MapRGB(screenSurface->format,c.r,c.g,c.b));
                    }
                }

                SDL_UpdateWindowSurface(window);

                char winttl[256];

                sprintf(winttl,"Sec: %i - Int: %u - X: %f - Y: %f",pl.cursec,totalI,pl.pos.x,pl.pos.y);

                SDL_SetWindowTitle(window,winttl);

                if(keys.f12)
                {
                    keys.f12 = false;
                    for(int i=0;i<sWidth*sHeight;i++)
                    {
                        zBufOut[i] = (uint8_t)std::clamp(zBuf[i]/4.0f,0.0f,255.0f);
                    }
                    std::ofstream zbuffile;
                    zbuffile.open("zbuf.raw", std::ofstream::binary);
                    zbuffile.write((char*)zBufOut,sWidth*sHeight);
                    zbuffile.close();
                }

                const auto endtime = clock.now();
                dtime = float(std::chrono::duration_cast<std::chrono::milliseconds>(endtime-starttime).count())/1000.0f;
            }
            SDL_DestroyWindow(window);
            SDL_Quit();
        }
    }
    return 0;
}